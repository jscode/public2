function FindProxyForURL (url, host) {

    if (dnsDomainIs(host, "facebook.com")
        || dnsDomainIs(host, "youtube.com")
        || dnsDomainIs(host, "googleblog.com")
        || dnsDomainIs(host, "google.com")
        || dnsDomainIs(host, "cloud.google.com")  
        || dnsDomainIs(host, "fonts.googleapis.com")
        || dnsDomainIs(host, "ajax.googleapis.com")
        || dnsDomainIs(host, "google.co.jp")
        || dnsDomainIs(host, "fonts.gstatic.com")
        || dnsDomainIs(host, "google-analytics.com")
        || dnsDomainIs(host, ".blogger.com")
        || dnsDomainIs(host, "rgc-deps.appspot.com")
        || dnsDomainIs(host, "ssl.google-analytics.com")
        || dnsDomainIs(host, "blogspot.com")
        || dnsDomainIs(host, "tensorflow.org")
        || dnsDomainIs(host, "katacontainers.io")
               ){
        return "PROXY 127.0.0.1:8887";
    }
    return "DIRECT";
}